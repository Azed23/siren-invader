import pm2 from 'pm2';

// import {splitCsvToJsonFiles} from "./csv_parser/split-into-multiple-files.js";

const json_files_dir = "json_files/"
let nb_cpus = 5
let nb_files = 25

/*console.log("Start - Splitting Csv Files into Json Files")
splitCsvToJsonFiles(json_files_dir)
console.log("End - Splitting Csv Files into Json Files")*/


function callWorkers() {
	
	pm2.connect((err) => {
		if (err) {
			process.exit(2)
		}
		
		pm2.start(
			{
				script: 'worker.js',
				name: 'WorkerCall',
				instances: 0
			},
			function(err, apps) {
				
				if (err) {
					process.exit(2);
				}
				
				let first_file_index = 1
				
				for (let i = 1; i <= nb_cpus; i++) {
					let last_file_index = first_file_index + 4
					pm2.sendDataToProcessId(
						i,
						{
							type: 'Enterprises Sirens',
							data: {
								"process_id": i,
								"json_files_dir": json_files_dir,
								"first_file_index": first_file_index,
								"last_file_index": last_file_index,
								"all_processes_last_file_index": nb_files
							},
							id: i,
							topic: 'Fill MongoDB with Enterprise sirens'
						},
						function (err, res) {}
					);
					first_file_index = last_file_index + 1
					
				}
			
			})
		
	});
	
	// Listening to a message from the worker
	pm2.launchBus(function(err, pm2_bus) {
		pm2_bus.on('process:msg', function(packet) {
			// When a process is finished
			if (packet.data.response === "OneProcessFinished") {
				pm2.stop(packet.data.proc_to_stop_id, function (err, proc) {
					if (err) {
						process.exit(2);
					}
				})
			}
			
			// When all processes are finished
			if (packet.data.response === "AllProcessesFinished") {
				pm2.disconnect()
			}
		})
	})
	
}


callWorkers()

