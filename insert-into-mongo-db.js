import fs from "fs";
import mongoose from 'mongoose';
import {useDefaultDB} from "./db_connection/mongo-connection.js";


const Schema = mongoose.Schema;
const ObjectId = Schema.ObjectId;


//=================================================================================================================
/*                                      Defintion of the Company Schema                                          */
const Company = new Schema({
	id: ObjectId,
	siren: String,
	nic: String,
	siret: String,
	dateCreationEtablissement: Date,
	dateDernierTraitementEtablissement: Date,
	typeVoieEtablissement: String,
	libelleVoieEtablissement: String,
	codePostalEtablissement: String,
	libelleCommuneEtablissement: String,
	codeCommuneEtablissement: String,
	dateDebut: Date,
	etatAdministratifEtablissement: String
});


//=================================================================================================================
/*                                      Defintion of the model                                                */
let companyModel = useDefaultDB().model('Company', Company, 'companies');



export const insertIntoDatabase = (jsonFile, cur_proc_last_file, all_proc_last_file, proc_id) => {
	
	let data = fs.readFileSync(jsonFile)
	let un_parsed_data =  data.toString()
	let parsed_data = JSON.parse(un_parsed_data)
	
	companyModel.collection.insertMany(parsed_data, function (err) {
		if (err) {
			console.log(err)
		} else {
			if (jsonFile === cur_proc_last_file) {
				process.send({
					type : 'process:msg',
					data : {
						response: "OneProcessFinished",
						proc_to_stop_id: proc_id
					}
				});
				console.log("Proc: " + proc_id + " has ended")
			}
			if (jsonFile === all_proc_last_file) {
				process.send({
					type : 'process:msg',
					data : {
						response: "AllProcessesFinished",
						proc_to_stop_id: proc_id
					}
				});
				console.log("PM2 Disconnected")
			}
			
		}
	})
	
}

