import mongoose from 'mongoose';
import fs from "fs";


let credentialsFile = fs.readFileSync("credentials.json")
let stringifiedData =  credentialsFile.toString()
let jsonData = JSON.parse(stringifiedData)

let url = "mongodb+srv://" + jsonData.user + ":" + jsonData.pass + "@" + jsonData.instance +"/"

const Schema = mongoose.Schema;


//=================================================================================================================
/*                                      Defintion of the Company Schema                                          */
const Company = new Schema({
	id: Schema.ObjectId,
	siren: String,
	nic: String,
	siret: String,
	dateCreationEtablissement: Date,
	dateDernierTraitementEtablissement: Date,
	typeVoieEtablissement: String,
	libelleVoieEtablissement: String,
	codePostalEtablissement: String,
	libelleCommuneEtablissement: String,
	codeCommuneEtablissement: String,
	dateDebut: Date,
	etatAdministratifEtablissement: String
});


// Listening a message from Main Process
process.on('message', function(packet) {
	
	let connection = mongoose.createConnection(url)
	
	connection.on('connected', () => {

		let connectedDb = connection.useDb("siren-invader-23")
		let CompanyModel = connectedDb.model('Company', Company, 'companies')
		
		if (packet.data.first_file_index !== undefined) {
			let start = packet.data.first_file_index
			let end = packet.data.last_file_index
			for (let i = start; i <= end; i++) {
				let jsonFile = packet.data.json_files_dir + "list_" + i + ".json"
				let cur_proc_last_file = packet.data.json_files_dir + "list_" + end + ".json"
				let all_proc_last_file = packet.data.json_files_dir + "list_" + packet.data.all_processes_last_file_index + ".json"
				let proc_id = packet.data.process_id
				
				let data = fs.readFileSync(jsonFile)
				let un_parsed_data =  data.toString()
				let parsed_data = JSON.parse(un_parsed_data)
				
				CompanyModel.collection.insertMany(parsed_data, function (err) {
					if (err) {
						process.exit(2)
					} else {
						if (jsonFile === cur_proc_last_file) {
							process.send({
								type : 'process:msg',
								data : {
									response: "OneProcessFinished",
									proc_to_stop_id: proc_id
								}
							});
							
						}
						if (jsonFile === all_proc_last_file) {
							process.send({
								type : 'process:msg',
								data : {
									response: "AllProcessesFinished",
									proc_to_stop_id: proc_id
								}
							});
							
						}
						
					}
				})
				
			}
			
		}
		
	});
	
});


