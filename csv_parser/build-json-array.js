
// [ 'Hobbies', 'First Name' ] [ 2, 1 ] To Read: CsvAttributesToRead
// [ 'First Name', 'Last Name', 'Hobbies' ] [ 0, 1, 2 ] Attributes List: CsvArrayData

export const getJsonArrayValues = (attributesOfGivenCsv, CsvArrayData, CsvAttributesToRead) => {

	if (CsvArrayData.length > 1 && attributesOfGivenCsv.length > 0) {
		
		let attributesPositions = []
		let foundValues = []
		let jsonValues = []
		
		// Detect the position of the csv headers
		for (let j = 0; j < CsvAttributesToRead.length; j++) {
			
			for (let i = 0; i < attributesOfGivenCsv.length; i++) {
				if (attributesOfGivenCsv[i] === CsvAttributesToRead[j]) {
					attributesPositions.push(i)
				}
			}
		}
		
		// Check if a value is empty and escapes it if so
		for (let i = 0; i < CsvArrayData.length; i++) {
			if (CsvArrayData[i] !== '') {
				let attributesValuesArray = []
				let lineSplit = CsvArrayData[i].split(",")
				
				for (const pos of attributesPositions) {
					attributesValuesArray.push(lineSplit[pos])
				}
				foundValues.push(attributesValuesArray)
			}
			
		}
		
		// Getting the values
		for (let i = 0; i < foundValues.length; i++) {
			let jsonModel = {}
			for (let j = 0; j < CsvAttributesToRead.length; j++) {
				
				if (typeof foundValues[i] !== "undefined" && foundValues[i][j] !== '') {
					jsonModel[CsvAttributesToRead[j]] = foundValues[i][j]
				}
				else {
					i++
				}
			}
			jsonValues.push(jsonModel)
		}
		
		return jsonValues
	}
}
