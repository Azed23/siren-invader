import readline from "readline";
import fs from "fs";
import {writeJsonFiles} from "./write-json-files.js";


const file = "../StockEtablissement_utf8.csv"
let numberOfLine = 0
let csvAttributesLine = ""


export const splitCsvToJsonFiles = (resultPath) => {
	
	let reader = readline.createInterface({
		input: fs.createReadStream(file),
		output: process.stdout,
		terminal: false
	});
	
	// Count the number of line in the file
	reader.on('line', function (currentLine) {
		if (numberOfLine === 0) {
			csvAttributesLine = currentLine
		}
		numberOfLine++
	})
	
	reader.on('close', function () {
		let numberOfSplitFileNeeded = 25
		let numberOfLineToRead = Math.floor(numberOfLine / numberOfSplitFileNeeded)
		
		writeJsonFiles(file, csvAttributesLine, numberOfLineToRead, resultPath)
		
	})
	
}

