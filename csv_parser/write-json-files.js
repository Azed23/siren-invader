import fs from "fs"
import readline from "readline";

import {getJsonArrayValues} from "./build-json-array.js";



// Returns array of attributes
export const getCsvAttributesList = (csvAttributesLine) => {
	
	if (typeof csvAttributesLine === "string" && csvAttributesLine.length > 0) {
		return csvAttributesLine.split(",")
	}
	
}


export const writeJsonFiles = (csvFile, csvAttributesLine, numberOfLinePerFile, resultPath) => {

	let reader = readline.createInterface({
		input: fs.createReadStream(csvFile),
		output: process.stdout,
		terminal: false
	});
	
	let index = 0
	let new_file_index = 1
	let listOfLines = []
	let givenCsvAttributes = getCsvAttributesList(csvAttributesLine)
	
	reader.on('line', function (line) {
		// Si index % numberOfLinePerFile == 0 alors il faut creer un nouveau fichier
		// parce que ca voudrait dire qu'on est a la fin du nombre de lignes autorise
		// par fichier pour le fichier actuel
		
		if (index !== 0) {
			
			if ((index % numberOfLinePerFile) === 0) {
				let new_file = resultPath + "list_" + new_file_index + ".json"
				let attributesToBeRead = ["siren", "nic", "siret", "dateCreationEtablissement", "dateDernierTraitementEtablissement",
					"typeVoieEtablissement", "libelleVoieEtablissement", "codePostalEtablissement", "libelleCommuneEtablissement",
					"codeCommuneEtablissement", "dateDebut", "etatAdministratifEtablissement"]
				let jsonArrayValues = getJsonArrayValues(givenCsvAttributes, listOfLines, attributesToBeRead)
				
				fs.writeFile(new_file, JSON.stringify(jsonArrayValues), err => {
					if (err) {
						console.log("Error")
					} else {
						console.log("Success")
					}
				})
				listOfLines = []
				new_file_index++
			}
			listOfLines.push(line)
		}
		
		index++
	})
	
	
	reader.on('close', function () {
		console.log("Files splitting is done!!!")
	})
	
}
